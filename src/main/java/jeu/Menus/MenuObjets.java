package jeu.Menus;
import jeu.ObjetsCombats.*;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.util.ArrayList;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
public class MenuObjets {
    private boolean loaded = false; // si faux utiliser afficherObjs, sinon loadedAffichage
    private Button currentButtonAttaque;
    private Button currentButtonDefense;
    private Scene loadedScene;
    
    public void afficherObjs(Stage stage, ObjetsCombat[] bonus, Scene scene){ 
        FlowPane menuObjets = new FlowPane();
        
        Scene sceneObjs = new Scene(menuObjets,800, 400);
        ObjetsCombatInit objetsInit = new ObjetsCombatInit();
        ArrayList<ObjetsCombat> objets = objetsInit.getObjs();
        for (int i=0; i<objetsInit.getTaille(); i++){
            int index = i;
            Button button = new Button();
            button.setOnAction(event -> {
                if (objets.get(index).getTypeAttaque()){
                    bonus[0] = objets.get(index);
                    if (this.currentButtonAttaque != null){
                        this.currentButtonAttaque.setStyle("-fx-border-color : light grey");
                    }
                    this.currentButtonAttaque = button;
                    button.setStyle("-fx-border-color : yellow");
                } else {
                    bonus[1] = objets.get(index);
                    if (this.currentButtonDefense != null){
                        this.currentButtonDefense.setStyle("-fx-border-color : light grey");
                    }
                    this.currentButtonDefense = button;
                    button.setStyle("-fx-border-color: yellow;");
                }
            });
            String imagePath = "/jeu/ObjetsCombats/Objets/" + objets.get(index).getImPath();
            URL imageURL = getClass().getResource(imagePath);
            Image image = new Image(imageURL.toExternalForm());
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth(100); 
            imageView.setFitHeight(100);
            StackPane stack = new StackPane();
            Label label = new Label("+ " + Integer.toString(objets.get(index).getBonus())+ (objets.get(index).getTypeAttaque() ? "atk" : "def"));
            label.setStyle("-fx-font-weight: bold;-fx-font-size: 16pt;");
            stack.getChildren().addAll(imageView,label);
            button.setGraphic(stack);
            menuObjets.getChildren().add(button);
        }
        Button Quitter = new Button("retour");
        Quitter.setOnAction(event -> {stage.setScene(scene);});
        menuObjets.getChildren().add(Quitter);
        stage.setScene(sceneObjs);
        this.loadedScene = sceneObjs;
        Image backgroundIm = new Image(getClass().getResource("/jeu/Menus/elements jpg/arriere-plan-menu.jpg").toExternalForm());
        BackgroundImage background = new BackgroundImage(backgroundIm,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT, 
            BackgroundPosition.CENTER, 
            new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, false));
        menuObjets.setBackground(new Background(background));
        this.loaded = true;
    }
    public void loadedAffichage(Stage stage){
        stage.setScene(this.loadedScene);
        if (this.currentButtonAttaque != null) {
            this.currentButtonAttaque.setStyle("-fx-border-color: yellow;");
        }
        if (this.currentButtonDefense != null) {
            this.currentButtonDefense.setStyle("-fx-border-color: yellow;");
        }
    }
    public boolean isLoaded(){
        return this.loaded;
    }
}
